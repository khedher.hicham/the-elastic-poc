# The Elastic POC
This is a POC that demonstrate how to deploy an elastic stack using Docker.  
The elasticsearch cluster contains 4 nodes (master, ingest, hot and cold), integrated with a snapshooting system using MinIO.

# Requirements
- A laptop with at least 4GB of RAM, in order to turn Docker locally.
- having Docker and docker-compose installed in your machine.

# How it works
## Start deployment
Launch the deployment of the elastic stack simply with :
```
docker-compose up

```
## Cluster Health Check
In order to check the health of the cluster, try those endpoints of the Cluster Health API.  
http://localhost:9200/_cat/health?v  
http://localhost:9200/_cat/nodes?v  
http://localhost:9200/_cat/allocation?v

## Authentication to Kibana
The X-pack extension is enabled in this cluster, so in order to connect into Kibana http://localhost:5601, you must enter the authentication credentials.
```
username: elastic
password: superpassword

```
## Create snapshots
* Connect to MinIO client in http://localhost:9000/minio with the authentication credentials
```
access_key: hichem
secret_key: minioroot

```
* Create a bucket and edit his policy by adding for example a "read and write" policy.
* Enter miniIO credentials in each node container, when containers are turning on as well, because those credentials are considered as a secure settings
```
bin/elasticsearch-keystore add s3.client.default.access_key
bin/elasticsearch-keystore add s3.client.default.secret_key

``` 
* Don't forget to reload secure settings by
```
POST _nodes/reload_secure_settings

```
* Get the internal IP of minIO container.
```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' minio

```
* Create a S3 repo with :
```
PUT _snapshot/minio-repo
{
  "type": "s3",
  "settings": {
    "bucket": "<my-bucket-name>",
    "endpoint": "<my-internal-minio-ip>:9000",
    "protocol" : "http"
  }
}

```
Finally, go to Kibana and create graphically a snapshot policy and then run it.  
In order to see some snapshots in minIO UI, don't forget to create idexes.
## Put down the cluster
```
docker-compose down -v

```
# Troubleshooting
For WSL users it's recommended to maximize the virtual memory of the docker desktop.
```
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144

```


